
// Selection Control Structure //

	// It sorts out whether the statement/s are to be executed based on the condition
	// whether it is true or false.

		// 1. If- Else
		// 2. Switch
		// 3. try catch finally statement

	// If-else Syntax:

	// if( condition )
	// {
		// statement
	// }
	// else
	// {
		// statement
	// }

	let numA = -1;

	if (numA < 0)
	{
		console.log("Hello");
	}

	let city = "New york";

	if (city === "New York")
	{
		console.log("Welcome to New York City");
	}

	// Else if

	let numB = 1;

	if (numA > 0)
	{
		console.log("Hello");
	}
	else if (numB > 0)
	{
		console.log("World");
	}

	city = "Tokyo";

	if (city === "New York")
	{
		console.log("Welcome to New York City");
	}
	else if (city === "Tokyo")
	{
		console.log("Welcome to Tokyo");
	}

	// Else Statement

	if (numA > 0)
	{
		console.log("Hello");
	}
	else if (numB === 0)
	{
		console.log("World");
	}
	else
	{
		console.log("Again");
	}

	let age = 20;

	if (age < 17)
	{
		console.log("Not allowed to drink!");
	}
	else
	{
		console.log("Shot puno");
	}

	// Mini Activity //

	let height;

	

	function heightReq(height)
	{
		if (height < 150)
		{
			console.log("Did not passed min height requirements: "+ height);
		}
		else if (height >= 150)
		{
			console.log("We passes the minimum height requirements: "+ height);
		}
	}
	let whatHeight = prompt("Height: ")
	heightReq(whatHeight);

	let message = "No Message";
	console.log(message);

	function determineTyphoonIntensity(windSpeed)
	{
		if (windSpeed < 30)
		{
			return "Not Typhoon yet";
		}
		else if (windSpeed <= 61)
		{
			return "Tropical Depression";
		}
		else if (windSpeed >= 62 && windSpeed <= 88)
		{
			return "Tropical Storm Detected";
		}
		else if (windSpeed >= 89 && windSpeed <= 117)
		{
			return "Severe Tropical Storm Detected";
		}
		else
		{
			return "Typhoon Detected";
		}
	}
	message = determineTyphoonIntensity(70);
	console.log(message);


if(message === "Tropical Storm Detected")
{
	console.warn(message);
	console.error(message);
}

// Truthy and Falsy //

	// In Javascript, a truthy  value is a value that is considered TRUE when
	// encountered in a Boolean context. Values are considered true unless
	// defined otherwise.

	// 1. false
	// 2. 0
	// 3. -0
	// 4. ""
	// 5. null
	// 6. undefined
	// 7. NaN

	// Truthy

	if (true)
	{
		console.log("Truthy");
	}

	if (1)
	{
		console.log("Truthy");
	}

	if ([])
	{
		console.log("Truthy");
	}

	// Falsy

	if (false)
	{
		console.log("Falsy");
	}

	if (0)
	{
		console.log("Falsy");
	}

	if (undefined)
	{
		console.log("Falsy");
	}

// Conditional (Ternary) Operator

	// Have an implicit return statement
	// Syntax:
	// (expression) ? ifTrue : ifFalse;


	// Single Statment Execution

	let ternaryResult = (1 < 18) ? "Statement is true" : "Statement is false";
	console.log("Result of ternary Operator: "+ ternaryResult);


// Multiple Statement Execution

	let name;

	function isOfLegalAge()
	{
		name = "John";
		return "You are of the legal age";
	}

	function isUnderAge()
	{
		name = "Jane";
		return "You are under the age limit";
	}

	let age1 = parseInt(prompt("What is your age?"));
	console.log(age1);

	let legalAge = (age1 > 18) ? isOfLegalAge() : isUnderAge();
	console.log("Result of ternary Operator in Functions: "+ legalAge + ", "+ name);

// Switch Statement

	// Syntax:
	// switch (expressions)
	// {
	// case value:
	// 	statement;
	// 	break;
	// case value:
	// 	statement
	// 	break;
	// default:
	// 	statement
	// 	break;
	// }

let day = prompt("What day of the week is it today?")
console.log(day);

switch(day)
{
case "Monday":
	console.log("The color of the day is red");
	break;
case "Tuesday":
	console.log("The color of the day is orane");
	break;
case "Wednesday":
	console.log("The color of the day is yellow");
	break;
case "Thursday":
	console.log("The color of the day is green");
	break;
case "Friday":
	console.log("The color of the day is blue");
	break;
case "Saturday":
	console.log("The color of the day is indigo");
	break;
case "Sunday":
	console.log("The color of the day is violet");
	break;
default:
	console.log("Plese input a valid day");
	break;
}

// try-catch Finally Statement

function showIntensityAlert(windSpeed)
{
	try
	{
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch (error)
	{
		console.log(error);
		console.log(error.message);
	}
	finally
	{
		alert("Intensity updates will show new alert.");
	}
}
showIntensityAlert(56);